OC.L10N.register(
    "integration_nepenthes",
    {
    "Cancel" : "Encaboxar",
    "Save" : "Guardar",
    "Copied!" : "¡Copióse!",
    "Copied to the clipboard" : "Copióse nel cartafueyu",
    "Details" : "Detalles",
    "Start typing to search" : "Comienza a escribir pa buscar",
    "Description" : "Descripción",
    "Create" : "Crear",
    "Failed to get Nepenthes notifications" : "Nun se puen consiguir los avisos d'Nepenthes",
    "Reset" : "Reafitar",
    "Invalid token" : "El pase ye inválidu",
    "Failed to save Nepenthes options" : "Nun se puen guardar les opciones d'Nepenthes",
    "Invalid key" : "La clave ye inválida",
    "URL is invalid" : "La URL ye inválida",
    "Response:" : "Rempuesta:",
    "Documentation" : "Documentación"
},
"nplurals=2; plural=(n != 1);");
