OC.L10N.register(
    "integration_nepenthes",
    {
    "Cancel" : "Cancelar",
    "Save" : "Guardar",
    "Copied!" : "¡Copiado!",
    "Copied to the clipboard" : "Copiado al portapapeles",
    "Details" : "Detalles",
    "Start typing to search" : "Empiece a escribir para buscar",
    "Description" : "Descripción",
    "Create" : "Crear",
    "Reset" : "Reiniciar",
    "Documentation" : "Documentación"
},
"nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;");
