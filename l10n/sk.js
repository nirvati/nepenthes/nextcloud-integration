OC.L10N.register(
    "integration_nepenthes",
    {
    "Nepenthes": "Nepenthes",
    "Error getting OAuth access token" : "Chyba pri získavaní prístupového tokenu OAuth",
    "Error getting OAuth refresh token" : "Chyba pri získavaní obnovovacieho tokenu OAuth",
    "Error during OAuth exchanges" : "Chyba počas výmeny OAuth",
    "Direct download error" : "Chyba pri priamom sťahovaní",
    "This direct download link is invalid or has expired" : "Tento priamy odkaz na stiahnutie je neplatný alebo jeho platnosť vypršala",
    "Bad HTTP method" : "Zlá metóda HTTP",
    "OAuth access token refused" : "Prístupový token OAuth bol zamietnutý",
    "Nepenthes Integration" : "Nepenthes integrácia",
    "Link Nextcloud files to Nepenthes work packages" : "Prepojte súbory Nextcloud s pracovnými balíkmi Nepenthes",
    "Nepenthes server" : "Nepenthes server",
    "Nepenthes host" : "Nepenthes server",
    "Edit server information" : "Upraviť informácie o serveri",
    "Cancel" : "Zrušiť",
    "Save" : "Uložiť",
    "Nepenthes OAuth settings" : "Nastavenia Nepenthes OAuth",
    "Replace Nepenthes OAuth values" : "Nahradiť hodnoty Nepenthes OAuth",
    "Nextcloud OAuth client" : "Klient Nextcloud Oauth",
    "Yes, I have copied these values" : "Áno, tieto hodnoty som skopíroval",
    "Replace Nextcloud OAuth values" : "Nahradiť hodnoty Nextcloud OAuth",
    "Copied!" : "Skopírované!",
    "Copy value" : "Kopírovať hodnotu",
    "Details" : "Podrobnosti",
    "Error connecting to Nepenthes" : "Chyba pri pripájaní k Nepenthes",
    "Could not fetch work packages from Nepenthes" : "Nepodarilo sa načítať pracovné balíky z Nepenthes",
    "Unexpected Error" : "Neočakávaná chyba",
    "No Nepenthes account connected" : "Nie je pripojený žiadny Nepenthes účet",
    "Start typing to search" : "Začnite písať pre vyhľadanie",
    "Search for a work package to create a relation" : "Ak chcete vytvoriť súvislosť, vyhľadajte pracovný balík",
    "Select a user or group" : "Vyberte používateľa alebo skupinu",
    "Description" : "Popis",
    "Create" : "Vytvoriť",
    "Mark as read" : "Označiť ako prečítané",
    "Failed to get Nepenthes notifications" : "Chyba pri získavaní upozornení z Nepenthes",
    "Existing relations:" : "Existujúce vzťahy:",
    "Confirm unlink" : "Potvrdiť odpojenie",
    "Unlink" : "Odpojiť",
    "Successfully connected to Nepenthes!" : "Úspešne pripojené k Nepenthes!",
    "OAuth access token could not be obtained:" : "Prístupový token OAuth sa nepodarilo získať:",
    "Nepenthes notifications" : "Notifikácie Nepenthes",
    "Nepenthes activity" : "Nepenthes aktivity",
    "_You have %s new notification in {instance}_::_You have %s new notifications in {instance}_" : ["Máte %s oznámenie v {instance}","Máte %s oznámenia v {instance}","Máte %s oznámení v {instance}","Máte %s oznámenia v {instance}"],
    "Connected accounts" : "Prepojené účty",
    "Please introduce your Nepenthes host name" : "Zadajte názov serveru kde prevádzkujete vašu inštanciu Nepenthes-u",
    "Reset" : "Resetovať",
    "Yes, replace" : "Áno, nahradiť",
    "Yes, reset" : "Áno, resetovať",
    "Nepenthes admin options saved" : "Nastavenia administrátora Nepenthesu boli uložené",
    "Failed to save Nepenthes admin options" : "Nepodarilo sa uložiť nastavenia administrátora Nepenthesu",
    "Connect to Nepenthes" : "Pripojiť k Nepenthes",
    "Enable navigation link" : "Povoliť navigačný odkaz",
    "Connected as {user}" : "Pripojený ako {user}",
    "Disconnect from Nepenthes" : "Odpojiť od Nepenthes",
    "Enable unified search for tickets" : "Zapnúť jednotné vyhľadávanie tiketov",
    "Warning, everything you type in the search bar will be sent to your Nepenthes instance." : "Varovanie, všetko čo napíšete do vyhľadávania bude odoslané do Nepenthes inštancie.",
    "Enable notifications for activity in my work packages" : "Povoliť upozornenia na aktivitu v mojich pracovných balíkoch",
    "Nepenthes options saved" : "Nastavenia Nepenthesu boli uložené",
    "Incorrect access token" : "Nesprávny prístupový token",
    "Invalid token" : "Neplatný token",
    "Nepenthes instance not found" : "Inštancia Nepenthes nebola nájdená",
    "Failed to save Nepenthes options" : "Nepodarilo sa uložiť nastavenia Nepenthesu",
    "Nepenthes integration" : "Nepenthes integrácia",
    "No Nepenthes notifications!" : "Žiadne notifikácie od Nepenthesu",
    "Invalid key" : "Neplatný kľúč",
    "Default user settings" : "Predvolené nastavenia užívateľa",
    "Server replied with an error message, please check the Nextcloud logs" : "Server vrátil chybovú hlášku, prosím skontrolujte logy Nextcloudu",
    "Documentation" : "Dokumentácia"
},
"nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);");
