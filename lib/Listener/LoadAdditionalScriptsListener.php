<?php

namespace OCA\Nepenthes\Listener;

use OC_Util;
use OCA\Files\Event\LoadAdditionalScriptsEvent;
use OCA\Nepenthes\AppInfo\Application;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\Util;

class LoadAdditionalScriptsListener implements IEventListener {
	public function __construct() {
	}

	public function handle(Event $event): void {
		if (!$event instanceof LoadAdditionalScriptsEvent) {
			return;
		}

		Util::addScript(Application::APP_ID, Application::APP_ID . '-filesPlugin');
	}
}
