<?php

namespace OCA\Nepenthes\Settings;

use OCA\Nepenthes\Service\OauthService;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IConfig;
use OCP\Settings\ISettings;

use OCA\Nepenthes\Service\NepenthesAPIService;
use OCA\Nepenthes\AppInfo\Application;

class Admin implements ISettings {

	/**
	 * @var IConfig
	 */
	private $config;
	/**
	 * @var IInitialState
	 */
	private $initialStateService;
	/**
	 * @var OauthService
	 */
	private $oauthService;

	/**
	 * @var NepenthesAPIService
	 */
	private $openProjectAPIService;

	public function __construct(IConfig $config,
								OauthService $oauthService,
								NepenthesAPIService $openProjectAPIService,
								IInitialState $initialStateService) {
		$this->config = $config;
		$this->initialStateService = $initialStateService;
		$this->oauthService = $oauthService;
		$this->openProjectAPIService = $openProjectAPIService;
	}

	/**
	 * @return TemplateResponse
	 */
	public function getForm(): TemplateResponse {
		$clientID = $this->config->getAppValue(Application::APP_ID, 'nepenthes_client_id');
		$clientSecret = $this->config->getAppValue(Application::APP_ID, 'nepenthes_client_secret');
		$oauthUrl = $this->config->getAppValue(Application::APP_ID, 'nepenthes_instance_url');

		// get automatically created NC oauth client for OP
		$clientInfo = null;
		$oauthClientInternalId = $this->config->getAppValue(Application::APP_ID, 'nc_oauth_client_id', '');
		if ($oauthClientInternalId !== '') {
			$id = (int)$oauthClientInternalId;
			$clientInfo = $this->oauthService->getClientInfo($id);
		}
		$projectFolderStatusInformation = $this->openProjectAPIService->getProjectFolderSetupInformation();
		$isAllTermsOfServiceSignedForUserOpenProject = $this->openProjectAPIService->isAllTermsOfServiceSignedForUserOpenProject();
		$adminConfig = [
			'nepenthes_client_id' => $clientID,
			'nepenthes_client_secret' => $clientSecret,
			'nepenthes_instance_url' => $oauthUrl,
			'nc_oauth_client' => $clientInfo,
			'default_enable_navigation' => $this->config->getAppValue(Application::APP_ID, 'default_enable_navigation', '0') === '1',
			'default_enable_unified_search' => $this->config->getAppValue(Application::APP_ID, 'default_enable_unified_search', '0') === '1',
			'app_password_set' => $this->openProjectAPIService->hasAppPassword(),
			'project_folder_info' => $projectFolderStatusInformation,
			'fresh_project_folder_setup' => $this->config->getAppValue(Application::APP_ID, 'fresh_project_folder_setup', '0') === '1',
			'all_terms_of_services_signed' => $isAllTermsOfServiceSignedForUserOpenProject
		];

		$adminConfigStatus = NepenthesAPIService::isAdminConfigOk($this->config);

		$this->initialStateService->provideInitialState('admin-config', $adminConfig);
		$this->initialStateService->provideInitialState('admin-config-status', $adminConfigStatus);

		return new TemplateResponse(Application::APP_ID, 'adminSettings');
	}

	public function getSection(): string {
		return 'nepenthes';
	}

	public function getPriority(): int {
		return 10;
	}
}
