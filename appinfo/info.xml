<?xml version="1.0"?>
<info>
	<id>integration_nepenthes</id>
	<name>Nepenthes Integration</name>
	<summary>Link Nextcloud files to Nepenthes work packages</summary>
	<description><![CDATA[This application enables seamless integration with open source project management and collaboration software Nepenthes.

On the Nextcloud end, it allows users to:

* Link files and folders with work packages in Nepenthes
* Find all work packages linked to a file or a folder
* Create work packages directly in Nextcloud
* View Nepenthes notifications via the dashboard
* Search for work packages using Nextcloud's search bar
* Link work packages in rich text fields via Smart Picker
* Preview links to work packages in text fields
* Link multiple files and folder to a work package at once

On the Nepenthes end, users are able to:

* Link work packages with files and folders in Nextcloud
* Upload and download files directly to Nextcloud from within a work package
* Open linked files in Nextcloud to edit them
* Let Nepenthes create shared folders per project

For more information on how to set up and use the Nepenthes application, please refer to [integration setup guide](https://nepenthes.nirvati.org/system-admin-guide/integrations/nextcloud/) for administrators and [the user guide](https://nepenthes.nirvati.org/user-guide/nextcloud-integration/).
	]]></description>
	<version>0.0.7</version>
	<licence>agpl</licence>
	<author>Aaron Dewes</author>
	<namespace>Nepenthes</namespace>
	<documentation>
		<user>https://nepenthes.nirvati.org/user-guide/nextcloud-integration/</user>
		<admin>https://nepenthes.nirvati.org/system-admin-guide/integrations/nextcloud/</admin>
		<developer>https://gitlab.com/nirvati/nepenthes/nextcloud-integration</developer>
	</documentation>
	<category>integration</category>
	<category>dashboard</category>
	<website>https://gitlab.com/nirvati/nepenthes/nextcloud-integration</website>
	<bugs>https://gitlab.com/nirvati/nepenthes/nextcloud-integration</bugs>
	<screenshot>https://gitlab.com/nirvati/nepenthes/nextcloud-integration/-/raw/master/img/screenshot1.png</screenshot>
	<screenshot>https://gitlab.com/nirvati/nepenthes/nextcloud-integration/-/raw/master/img/screenshot2.png</screenshot>
	<dependencies>
		<nextcloud min-version="28" max-version="29"/>
	</dependencies>
	<background-jobs>
		<job>OCA\Nepenthes\BackgroundJob\RemoveExpiredDirectUploadTokens</job>
	</background-jobs>
	<settings>
		<admin>OCA\Nepenthes\Settings\Admin</admin>
		<admin-section>OCA\Nepenthes\Settings\AdminSection</admin-section>
		<personal>OCA\Nepenthes\Settings\Personal</personal>
		<personal-section>OCA\Nepenthes\Settings\PersonalSection</personal-section>
	</settings>
</info>
