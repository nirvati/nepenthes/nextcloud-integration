import './bootstrap.js'

OCA.Files.fileActions.registerAction({
	name: 'open-project',
	displayName: t('integration_nepenthes', 'Nepenthes'),
	mime: 'all',
	permissions: OC.PERMISSION_READ,
	iconClass: 'icon-nepenthes',
	actionHandler: (filename, context) => {
		const fileList = context.fileList
		if (!fileList._detailsView) {
			return
		}
		// use the sidebar-tab id for the navigation
		fileList.showDetailsView(filename, 'open-project')
	},
})
